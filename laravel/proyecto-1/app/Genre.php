<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    //protected $table = 'genres';

    //public $timestamps = true;

    //protected $dates = ['created_at', 'updated_at'];

    public function getActiveToString()
    {
    	return $this->active ? 'Está activo' : 'Está oculto';
    }

    public function scopeOrYear2016($query)
    {
    	return $query->orWhere(function ($query) {
			$query->where('created_at', '>=', '2016-01-01')
				->where('created_at', '<', '2017-01-01')
			;
		});
    }
}
