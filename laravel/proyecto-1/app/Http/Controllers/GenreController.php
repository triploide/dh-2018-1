<?php

namespace App\Http\Controllers;

use App\Genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    public function index()
    {
    	$genres = Genre::all();
    	return view('genres.index', compact('genres'));
    }

    public function show(int $id)
    {
    	$genre = Genre::find($id);
    	//$genre = Genre::where('id', $id)->first();
    	return view('genres.show', compact('genre'));
    }

    public function test()
    {
    	$genres = Genre::where('ranking', '>', 5)->orYear2016()->get();
    	dd($genres->toArray());
    }
}
