<?php

namespace App\Http\Controllers;

use App\Genre;
use App\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function index()
    {
    	$movies = Movie::all();
    	return view('admin.movies.index', compact('movies'));
    }

    public function create()
    {
    	$genres = Genre::orderBy('name')->pluck('name', 'id');
        $movie = new Movie;
    	return view('admin.movies.form', ['genres' => $genres, 'movie' => $movie]);
    }

    public function store()
    {
    	/*
    	$movie = new Movie;
    	$movie->title = request()->input('title');
    	$movie->length = request()->input('length');
    	$movie->rating = request()->input('rating');
    	$movie->awards = request()->input('awards');
    	$movie->release_date = request()->input('release_date');
    	$movie->genre_id = request()->input('genre_id');
    	$movie->save();
    	*/

    	/*
    	$movie = new Movie(request()->all());
    	$movie->save();
    	*/

    	//create
    	$movie = Movie::create(request()->all());

    	//update
    	//$movie = Movie::find(58);
    	//$movie->update(request()->all());

    	return redirect('admin/movies');
    }

    public function edit($id)
    {
        $genres = Genre::orderBy('name')->pluck('name', 'id');
        $movie = Movie::find($id);
        return view('admin.movies.form', ['genres' => $genres, 'movie' => $movie]);
    }
}
