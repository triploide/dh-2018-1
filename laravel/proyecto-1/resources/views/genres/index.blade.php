<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Listado de géneros</title>
</head>
<body>
	<h1>Géneros</h1>

	<ul>
		@foreach ($genres as $genre)
			<li>
				<a href="/generos/{{$genre->id}}">{{ $genre->name }}</a>
				- {{ $genre->getActiveToString() }} ({{ $genre->created_at->toFormattedDateString() }})
			</li>
		@endforeach
	</ul>

</body>
</html>