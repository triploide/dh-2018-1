<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Formulario</title>
</head>
<body>
	<h1>Crear fila de la tabla</h1>

	@if ($errors->count())
		@foreach ($errors->all() as $error)
			<p>{{ $error }}</p>
		@endforeach
	@endif
	
	<form action="/ejemplos/request/form" method="post">
		@csrf
		<lable>Nombre</lable>
		<input type="text" name="nombre" value="{{ old('nombre') }}">
		<br><br>
		<lable>Email</lable>
		<input type="text" name="email" value="{{ old('email') }}">
		<br><br>
		<lable>Password</lable>
		<input type="password" name="password" value="{{ old('password') }}">
		<br><br>
		<lable>Confirmación de Password</lable>
		<input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}">
		<br><br>
		<input type="submit" value="Enviar" name="enviador">
	</form>
</body>
</html>