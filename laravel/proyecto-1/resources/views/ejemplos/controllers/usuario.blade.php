<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Vista</title>
</head>
<body>
  @if (count($datos))
    <p>El array tiene datos</p>
  @else
    <p>El array no tiene datos</p>
  @endif
  
  {{--
  @foreach ($datos as $key => $value)
    <p>El key es: {{ $key }} - El value es: {{ $value }}</p>
  @endforeach
  --}}
  
  @forelse ($datos as $key => $value)
    <p>El key es: {{ $key }} - El value es: {{ $value }}</p>
  @empty
    <p>No hay resultados</p>
  @endforelse
  
  {{ $email ?? 'no hay mail' }}
  
  <h1>Hola {{ $usuario }}</h1>
</body>
</html>