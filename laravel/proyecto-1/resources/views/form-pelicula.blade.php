@extends('app')

@section('contenido')
  <form class="" action="/formularios/agregarPelicula" method="post">
    {{ csrf_field() }}
    <p>
      <label for="nombre">Nombre</label>
      <input name="nombre" type="text" id="nombre">
    </p>
    <p>
      <input type="submit" value="Enviar">
    </p>
  </form>
@endsection
