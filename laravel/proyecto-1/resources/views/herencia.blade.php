@extends('app')

@section('title', 'Título de herencia')

@section('nav')
  @parent
  <a href="#">Link del hijo</a>
@endsection

@section('titulo')
  <h1>Hola soy herencia</h1>
@endsection

@section('subtitulo')
  <h2>Subtítulo de herencia</h2>
  @parent
@endsection