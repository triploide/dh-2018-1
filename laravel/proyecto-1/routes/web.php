<?php

Route::get('/', function () {
  return view('welcome');
});

//Route::view('/', 'welcome');
//Route::resource('movies', 'MovieController');
Route::get('admin/movies', 'MovieController@index');
Route::get('admin/movies/create', 'MovieController@create');
Route::post('admin/movies', 'MovieController@store');
Route::get('admin/movies/{id}/edit', 'MovieController@edit');

//----------------------------------------------------------------

/**************
*****Rutas*****
***************/
Route::get('ejemplos/rutas/usuarios/registro', function () {
  return '<h1>Registro de usuario</h1>';
});

Route::get('ejemplos/rutas/usuarios/{index?}', function ($index=0) {
  $usuarios = ['Usuario', 'Barry','Barclay','Bert','Bort'];
  return "Hola " . $usuarios[$index];
});

Route::get('ejemplos/rutas/juguetes/{subcategoria}/{producto}', function ($subcategoria, $producto) {
  return "$subcategoria $producto";
});

Route::get('ejemplos/rutas/numeros/{num1}/{num2?}', function ($num1, $num2=null) {
  $resultado;
  if ($num2 == null) {
    $resultado = ($num1 % 2 == 0) ? 'Es par' : 'Es impar';
  } else {
    $resultado = $num1 * $num2;
  }
  return $resultado;
});

/******************************
*****Rutas con controllers*****
*******************************/
Route::get('ejemplos/controllers/usuarios/{index?}', 'UserController@show');

/**************
*****Blade*****
***************/
Route::get('ejemplos/blade/home', 'BladeController@home');
Route::get('ejemplos/blade/herencia', 'BladeController@herencia');

/**************
*****Form******
***************/
Route::get('ejemplos/formularios/agregarPelicula', 'FormController@mostrarForm');
Route::post('ejemplos/formularios/agregarPelicula', 'FormController@guardarPelicula');

/*****************
*****Eloquent*****
******************/
Route::get('ejemplos/eloquent/generos', 'GenreController@index');
Route::get('ejemplos/eloquent/generos/test', 'GenreController@test');
Route::get('ejemplos/eloquent/generos/{id}', 'GenreController@show');

/*****************
*****Requests*****
******************/
Route::get('ejemplos/request/form', 'RequestController@create');
Route::post('ejemplos/request/form', 'RequestController@store');