<?php
require 'vendor/autoload.php';

use Carbon\Carbon;

$now = Carbon::now();

echo $now->format('d-m-Y');
