<?php
require '../conn.php';

//FECTH ALL
echo '<h3>Fetch all</h3>';
$stmt = $db->prepare('SELECT * FROM movies LIMIT 3');
$stmt->execute();
$movies = $stmt->fetchAll(PDO::FETCH_ASSOC);
var_dump($movies);

//FETCH 
echo '<hr>';
echo '<h3>Fetch</h3>';
$stmt = $db->prepare('SELECT * FROM movies');
$stmt->execute();
$movies = $stmt->fetch(PDO::FETCH_ASSOC);
var_dump($movies);

//FETCH COLUMN
echo '<hr>';
echo '<h3>Fetch Column</h3>';
$stmt = $db->prepare('SELECT title FROM movies');
$stmt->execute();
$movies = $stmt->fetchColumn();
var_dump($movies);

