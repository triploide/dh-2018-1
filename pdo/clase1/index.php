<?php require '../conn.php'; ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Home - RAM</title>
  </head>
  <body>
    <h1>Estrenos</h1>
    <?php
    $query = $db->query('SELECT title FROM movies ORDER BY release_date DESC LIMIT 4');
    ?>
    
    <?php while ($titulo = $query->fetchColumn()): ?>
      <p><?php echo $titulo ?></p>
    <?php endwhile; ?>
  </body>
</html>