<?php

class Genre extends Model
{
  public $table='genres';
  public $columns = ['name', 'ranking', 'active'];
}
