<?php

class DB
{
  public static function find($id, $model)
  {
    $model = new $model();

    $db = new PDO('mysql:host=localhost;dbname=movies;charset=utf8', 'root', 'root', [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

    $stmt = $db->prepare('SELECT * FROM '.$model->table. ' WHERE id = :id');
    $stmt->execute([':id' => $id]);
    $columns = $stmt->fetch(PDO::FETCH_ASSOC);

    foreach ($columns as $key => $value) {
      if (in_array($key, $model->columns)) {
        $model->$key = $value;
      }
    }

    return $model;
  }
}
