<?php

abstract class Model
{
  public $db;
  public $table = '';
  public $columns = [];
  public $data = [];
  public $id;
  
  public function __construct()
  {
    $this->db = new PDO('mysql:host=localhost;dbname=movies;charset=utf8', 'root', 'root', [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
  }
  
  public function insert(array $datos)
  {
    $columns = [];
    $values = [];
    foreach ($datos as $key => $value) {
      if (in_array($key, $this->columns)) {
        $columns[] = $key;
        $values[] = '"'.$value.'"';
      }
    }
    
    $columns = implode(', ', $columns);
    $values = implode(', ', $values);
    
    $stmt = $this->db->prepare("INSERT INTO ".$this->table." (".$columns.") VALUES (".$values.") ");
    $stmt->execute();
    
    //$this->id = $stmt->lastInsertId();
  }
  
  public function update(array $datos)
  {
    return false;
  }
  
  public function delete()
  {
    return false;
  }

  public function __get($name)
  {
    if (in_array($name, $this->columns)) {
      return $this->data[$name];
    }
  }

  public function __set($name, $value)
  {
    if (in_array($name, $this->columns)) {
      $this->data[$name] = $value;
    }
  }
}
