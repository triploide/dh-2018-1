<?php require 'classes/Color.php'; ?>
<?php require 'classes/Bus.php'; ?>
<?php
$bus = new Bus;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Colores</title>
</head>
<body>
  <h1 style="color:<?php echo Color::ROJO; ?>">Hola mundo</h1>
  <h3>El costo es <?php echo $bus->calcularValor(12)   ?></h3>
</body>
</html>