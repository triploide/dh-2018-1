<?php
declare(strict_types = 1);
require 'classes/Usuario.php';
require 'classes/Fecha.php';

// $juan = new Usuario('Juan', 'Perez');
// $juan->saludar();

$fecha = new Fecha();
$resultado = $fecha->restar('1990-01-01', '2010-01-01');

$lalo = new Usuario('Lalo', 'Landa');
$lalo->setPassword('1234');

$lalo->setEdad($resultado);

var_dump($lalo);

