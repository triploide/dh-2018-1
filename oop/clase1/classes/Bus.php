<?php 

class Bus {
  
  const PRECIO_MENOR = 12;
  const PRECIO_MAYOR = 15;
  
  public function calcularValor($distancia)
  {
    if ($distancia <= 10) {
      return Bus::PRECIO_MENOR;
    } else {
      return Bus::PRECIO_MAYOR;
    }
  }
  
}