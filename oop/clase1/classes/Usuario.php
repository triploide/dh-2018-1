<?php
declare(strict_types = 1);

class Usuario
{
  private $nombre;
  public $apellido;
  private $password;
  private $fecha_nacimiento;
  private $edad;
  
  public function __construct(String $nombre, String $apellido)
  {
    $this->nombre = $nombre;
    $this->apellido = $apellido;
  }
  
  public function getNombre()
  {
    return $this->nombre;
  }
  
  public function setNombre($nombre)
  {
    $this->nombre = $nombre;
  }
  
  public function getPassword()
  {
      return $this->password;
  }
  
  public function setPassword($pass)
  {
    $this->password = password_hash($pass, PASSWORD_DEFAULT);
  }
  
  public function agregarAmigo(Usuario $amigo)
  {
    # code...
  }
  
  public function setEdad(int $edad)
  {
      $this->edad = $edad;
  }
  
  public function saludar()
  {
    echo 'Hola soy '.$this->nombreCompleto();
  }
  
  public function nombreCompleto(): String
  {
      return $this->nombre . ' ' . $this->apellido.'<br>';
  }
}
