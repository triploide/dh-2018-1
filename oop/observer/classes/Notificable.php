<?php

interface Notificable
{
  public function suscribir($suscriptor);
  
  public function desuscribir($suscriptor);
  
  public function notificar();
}

