<?php

class CampamentoMaderero extends Edificio implements Notificable
{
  
  private $suscriptores = [];
  
  private $cantidadDeMadera = 0;
  
  public function getCantidadDeMadera()
  {
    return $this->cantidadDeMadera;
  }
  
  public function setCantidadDeMadera($cantidad)
  {
    $this->cantidadDeMadera += $cantidad;
    $this->notificar();
  }
  
  public function suscribir($suscriptor)
  {
    $this->suscriptores[] = $suscriptor;
  }
  
  public function desuscribir($suscriptor)
  {
    # code...
  }
  
  public function notificar()
  {
    foreach ($this->suscriptores as $suscriptor) {
      $suscriptor->actualizar($this);
    }
  }
}