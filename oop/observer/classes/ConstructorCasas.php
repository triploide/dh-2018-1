<?php
/**
 * 
 */
class ConstructorCasas implements Actualizable
{
  
  private $cantidadDeMadera = 0;
  
  public function actualizar($camp)
  {
    $this->cantidadDeMadera = $camp->getCantidadDeMadera();
  }
  
  public function suscribir($camp)
  {
    $camp->suscribir($this);
  }

}
