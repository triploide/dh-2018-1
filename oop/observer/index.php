<?php

require 'classes/Notificable.php';
require 'classes/Edificio.php';
require 'classes/CampamentoMaderero.php';
require 'classes/Actualizable.php';
require 'classes/ConstructorCasas.php';
require 'classes/ConstructorBarcos.php';

$camp = new CampamentoMaderero;
$constructorCasas = new ConstructorCasas;
$constructorBarcos = new ConstructorBarcos;

$constructorCasas->suscribir($camp);
$constructorBarcos->suscribir($camp);

$camp->setCantidadDeMadera(10);

var_dump($constructorCasas);
var_dump($constructorBarcos);

