<?php

class Rol extends UnidadDePermiso
{
  
  public $permisos = [];
  
  public function puedo($accion)
  {
    $puede = false;
    foreach ($this->permisos as $permiso) {
      if ($permiso->puedo($accion)) {
        $puede = true;
        break;
      }
    }
    return $puede;
  }
  
  public function agregarPermiso(UnidadDePermiso $permiso)
  {
    $this->permisos[] = $permiso;
  }
}