<?php
require 'Usuario.php';
require 'UnidadDePermiso.php';
require 'Rol.php';
require 'Permiso.php';

$rolAdmin = new Rol('Admin');

$crear = new Permiso('crear');
$editar = new Permiso('editar');
$borrar = new Permiso('borrar');

$rolAdmin->agregarPermiso($crear);
$rolAdmin->agregarPermiso($editar);
$rolAdmin->agregarPermiso($borrar);

echo '<h1>Juan</h1>';

$juan = new Usuario('Juan');

$crear = new Permiso('crear');
$editar = new Permiso('editar');

$juan->agregarPermiso($crear);
$juan->agregarPermiso($editar);

var_dump( $juan->puedo('crear') );

var_dump( $juan->puedo('editar') );

var_dump( $juan->puedo('borrar') );

echo '<h1>Admin</h1>';

$admin = new Usuario('Admin');
$admin->agregarPermiso($rolAdmin);

var_dump( $admin->puedo('crear') );

var_dump( $admin->puedo('editar') );

var_dump( $admin->puedo('borrar') );
