<?php

class Usuario
{
  private $nombre;
  
  private $permisos = [];
  
  public function __construct($nombre)
  {
    $this->nombre = $nombre;
  }
  
  public function puedo($accion)
  {
    $puedo = false;
    foreach ($this->permisos as $permiso) {
      if ($permiso->puedo($accion)) {
        $puedo = true;
        break;
      }
    }
    return $puedo;
  }
  
  public function agregarPermiso($permiso)
  {
    $this->permisos[] = $permiso;
  }
}