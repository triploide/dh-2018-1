<?php

class Permiso extends UnidadDePermiso
{  
  public function puedo($accion)
  {
    return $accion == $this->nombre;
  }
}