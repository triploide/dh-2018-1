<?php

require 'lib/Intervention/Imagen.php';
require 'classes/Helpers/Validator.php';
require 'classes/Imagen.php';
require 'classes/Pelicula.php';

use Lib\Intervention\Imagen as Intervention;
use RAM\Imagen;

$intervention = new Intervention;
$intervention->crop(600, 400);

$validator = new RAM\Helpers\Validator;

$imagen = new Imagen;
$imagen->setSrc('perro.jpg')->setDescription('Perro');

$pelicula = new RAM\Pelicula('Perros 2');
$pelicula->setImagen($imagen);
echo $pelicula->bannerToHtml();
