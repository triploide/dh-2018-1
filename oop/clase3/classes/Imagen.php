<?php
namespace RAM;

class Imagen 
{
  private $src;
  private $description;
  
  public function getSrc()
  {
    return $this->src;
  }
  
  public function setSrc($src)
  {
    $this->src = $src;
    return $this;
  }
  
  public function getDescription()
  {
    return $this->description;
  }
  
  public function setDescription($description)
  {
    $this->description = $description;
    return $this;
  }
}