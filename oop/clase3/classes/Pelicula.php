<?php
namespace RAM;

use \RAM\Helpers\Validator;

class Pelicula
{
  private $titulo;
  private $imagen;
  private $validator;
  
  public function __construct($titulo)
  {
    $this->titulo = $titulo;
    $this->validator = new Validator;
  }
  
  public function getTitulo()
  {
    return $this->titulo;
  }
  
  public function setTitulo($titulo)
  {
    $this->titulo = $titulo;
  }
  
  public function setImagen(Imagen $imagen)
  {
    $this->imagen = $imagen;
  }
  
  public function bannerToHtml()
  {
    return '<img src="'.$this->imagen->getSrc().'" alt="'.$this->imagen->getDescription().'" />';
  }
}