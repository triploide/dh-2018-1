<?php

class Leniador extends Aldeano
{
  private $carga;
  
  public function __construct($vida, $ataque, $carga)
  {
    parent::__construct($vida, $ataque);
    $this->carga = $carga;
  }
  
  public function atacar($oponente)
  {
    parent::atacar($oponente);
    $i = rand(1,3);
    if ($i == 2) {
      echo '¡hachazoooo!';
      $oponente->setVida( $oponente->getVida() - 10 );
    }
  }
  
  public function mostrarme()
  {
    //echo '<p> Carga: '.$this->carga .'</p>';
    echo '<p> Vida: '.$this->vida .'<br>Ataque'.$this->ataque.'</p>';
  }
  
  public function conseguirTrabajo()
  {
    $herramientas = ['Hacha', 'Sierra'];
    $i = rand(0,count($herramientas)-1);
    $this->herramienta = $herramientas[$i];
  }
  
}
