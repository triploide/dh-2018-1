<?php

class BusHijo extends Bus
{
  public static function calcularCosto($value)
  {
    if ($value < 10) {
      return self::$precios[0];
    } elseif($value < 20) {
      return self::$precios[1];
    } else {
      return self::$precios[2];
    }
  }
}