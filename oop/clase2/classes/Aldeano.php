<?php

abstract class Aldeano
{
  use Mover;
  
  protected $vida;
  protected $ataque;
  protected $herramienta;
  protected $velocidad;
  
  public function __construct($vida, $ataque)
  {
    $this->setVida($vida);
    $this->ataque = $ataque;
  }
  
  public function getVida()
  {
    return $this->vida;
  }
  
  public function setVida($vida)
  {
    $this->vida = (int) $vida;
  }
  
  public function atacar($oponente)
  {
    $oponente->setVida( $oponente->getVida() - $this->ataque );
  }
  
  abstract public function conseguirTrabajo();
  
}
