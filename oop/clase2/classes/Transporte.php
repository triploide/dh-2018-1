<?php

interface Transporte
{
  public function mover(int $x, int $y);
}
