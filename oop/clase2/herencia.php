<?php

require 'classes/Aldeano.php';
require 'classes/Leniador.php';

//$oponente = new Aldeano(100, 5);

$leniador = new Leniador(100, 5, 5);
$leniador2 = new Leniador(100, 5, 5);

$leniador->atacar($leniador2);
while ($leniador->getVida() > 0 && $leniador2->getVida() > 0) {
  $leniador2->atacar($leniador);
  $leniador->atacar($leniador2);
}

var_dump($leniador, $leniador2);

//var_dump($leniador2 instanceof Aldeano);