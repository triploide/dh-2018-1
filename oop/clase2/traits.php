<?php

require 'classes/Mover.php';
require 'classes/Transporte.php';
require 'classes/OtroTrait.php';
require 'classes/Bus.php';
require 'classes/Aldeano.php';
require 'classes/Leniador.php';

$bus = new Bus;
$bus->hacer();
$bus->mover(4, 8);

echo '<br>';
$aldeano = new Leniador(3,3,3);
$aldeano->mover(4, 8);