<?php

function guardarArchivo($inputName, $dir, $nombre)
{
  $origen = $_FILES[$inputName]['tmp_name'];

  $ext = pathinfo($_FILES[$inputName]['name'], PATHINFO_EXTENSION);

  $destino = $dir . $nombre . '.' . $ext;
  
  move_uploaded_file($origen, $destino);
  
  return $nombre . '.' . $ext;
}

function getAllMovies() {
  $peliculas = file_get_contents(__DIR__ . '/../db/movies.json');
  $peliculas = json_decode($peliculas, true);
  if (!$peliculas) {
    $peliculas = [];
  }
  return $peliculas;
}
