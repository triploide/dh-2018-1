<?php
require '../helpers.php';

$db = new PDO('mysql:host=localhost;dbname=movies;charset=utf8', 'root', 'root', [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

$params = ['title', 'rating', 'release_date', 'awards', 'length', 'genre_id'];

if ($_POST['id']) {
  $query = '';
  foreach ($params as $param) {
    $query .= $param.'=:'.$param.',';
  }
  $query = trim($query, ',');
  $stmt = $db->prepare('UPDATE movies SET '.$query.' WHERE id = :id');
  $stmt->bindValue(':id', $_POST['id']);
} else {
  $stmt = $db->prepare('INSERT INTO movies ('.implode($params, ', ').') VALUES (:'.implode($params, ', :').')');
}

foreach ($params as $param) {
  $stmt->bindValue(':'.$param, $_POST[$param]);
}

$stmt->execute();





$stmt = $db->prepare('INSERT INTO movies (title, rating, release_date) VALUES (:title, :rating, :release_date)');

$stmt->bindValue(':title', $_POST['title']);
$stmt->bindValue(':rating', $_POST['rating']);
$stmt->bindValue(':release_date', $_POST['release_date']);

$stmt->execute();













