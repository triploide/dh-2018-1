<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Movie - REM Admnin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Brand</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <ul class="nav nav-pills nav-stacked">
            <li role="presentation" class="active"><a href="#">Home</a></li>
            <li role="presentation"><a href="#">Profile</a></li>
            <li role="presentation"><a href="#">Messages</a></li>
          </ul>
        </div>
        <div class="col-md-9">
          <?php if (isset($_GET['movieId'])): ?>
            <h1>Editar Película</h1>
          <?php else: ?>
            <h1>Nueva Película</h1>
          <?php endif; ?>
          <form action="../php/controllers/movie.controller.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label for="titulo">Título</label>
              <input class="form-control" type="text" name="title" value="">
            </div>
            <div class="form-group">
              <label for="titulo">Duración</label>
              <input class="form-control" type="text" name="length" value="">
            </div>
            <div class="form-group">
              <label for="titulo">Rating</label>
              <input class="form-control" type="text" name="rating" value="">
            </div>
            <div class="form-group">
              <label for="titulo">Premios</label>
              <input class="form-control" type="text" name="awards" value="">
            </div>
            <div class="form-group">
              <label for="titulo">Estreno</label>
              <input class="form-control" type="date" name="release_date" value="">
            </div>
            <div class="form-group">
              <label for="banner">Banner</label>
              <input class="form-control" type="file" name="banner" value="">
            </div>
            <div class="form-group">
              <label for="titulo">Géneros</label>
              <select name="genre_id" id="" class="form-control">
                <option value="1">Comedia</option>
                <option value="2">Terror</option>
                <option value="3">Drama</option>
              </select>
            </div>
            <?php $id = (isset($_GET['movieId'])) ? $_GET['movieId'] : '' ?>
            <input type="hidden" name="id" value="<?php echo $id ;?>">
            <div class="form-group">
              <button class="btn btn-primary" type="submit" name="button">Cargar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>