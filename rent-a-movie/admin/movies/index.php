<?php
require '../php/helpers.php';
$movies = getAllMovies();
$categorias = [
  1 => 'Acción',
  2 => 'Comedia',
  3 => 'Drama'
];
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Movie - REM Admnin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Brand</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <ul class="nav nav-pills nav-stacked">
            <li role="presentation" class="active"><a href="#">Home</a></li>
            <li role="presentation"><a href="#">Profile</a></li>
            <li role="presentation"><a href="#">Messages</a></li>
          </ul>
        </div>
        <div class="col-md-9">
          <h1>Listado de Películas</h1>
          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Título</th>
                <th>Categoría</th>
                <th>Editar</th>
              </tr>
            </thead>
<tbody>
  <?php foreach ($movies as $movie): ?>
    <tr>
      <td><?php echo $movie['titulo'] ?></td>
      <td><?php echo $categorias[$movie['categoria']]  ?></td>
      <td>
        <a class="btn btn-primary" href="form.php?movieId=<?php echo $movie['id'] ?>">
          <span class="glyphicon glyphicon-pencil"></span>
        </a>
        <a class="btn btn-danger" href="#">
          <span class="glyphicon glyphicon-trash"></span>
        </a>
      </td>
    </tr>
  <?php endforeach; ?>
</tbody>
          </table>
        </div>
      </div>
    </div>
  </body>
</html>