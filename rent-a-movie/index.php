<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Rent a Movie</title>
    <meta name="description" content="Todas las películas en un solo lugar">
    <?php include 'tpl/partials/head.tpl'; ?>
</head>
<body>
    <?php include 'tpl/partials/navbar.tpl'; ?>

    <main class="container">
        <!-- estreno -->
        <section class="row">
            <article class="col-md-3">
                <img src="http://lorempixel.com/200/300/technics/1" style="width: 100%" />
                <h3>Lorem ipsum dolor sit amet</h3>
            </article>
            <article class="col-md-3">
                <img src="http://lorempixel.com/200/300/technics/2" style="width: 100%" />
                <h3>Consectetur adipisicing elit</h3>
            </article>
            <article class="col-md-3">
                <img src="http://lorempixel.com/200/300/technics/3" style="width: 100%" />
                <h3>Earum officiis facere eveniet.</h3>
            </article>
            <article class="col-md-3">
                <img src="http://lorempixel.com/200/300/technics/4" style="width: 100%" />
                <h3>Quisquam ratione aliquam blanditiis</h3>
            </article>
        </section>
        <!-- /estrenos -->
    </main>

    <?php include 'tpl/partials/footer.tpl'; ?>

    <?php include 'tpl/partials/scripts.tpl'; ?>

</body>
</html>