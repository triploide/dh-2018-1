<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h4>RAM - Rent a Movie</h4>
                <p>2018. © Patente pendiente.</p>
            </div>
            <div class="col-md-4">
                <h4 class="widget-title text-center">Subcripción<small><br>Recibí todo el spam que estabamos esperando y más.</small></h4>
                <form action="?" method="post">
                    <input class="form-control" type="email" name="email" placeholder="Email">
                </form>
            </div>
            <div class="col-md-4">
                <ul>
                    <li><a href="#">Placeat minus</a></li>
                    <li><a href="#">Consectetur adipisicing</a></li>
                    <li><a href="#">Dolor sit amet</a></li>
                    <li><a href="#">Lorem ipsum</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>