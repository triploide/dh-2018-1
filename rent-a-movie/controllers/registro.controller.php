<?php

function subirArchivo($inputName, $path, $nombre=null)
{
  if ($_FILES[$inputName]['error'] == UPLOAD_ERR_OK) {
    $dir = $path;
    $nombreImagen = ($nombre == null) ? uniqid() : $nombre;
    $ext = pathinfo($_FILES[$inputName]['name'], PATHINFO_EXTENSION);
    $nombreImagen .= '.' . $ext;
    $destino = $dir . $nombreImagen;
    move_uploaded_file($_FILES[$inputName]['tmp_name'], $destino);
  }
  return $nombreImagen;
}

$nombreImagen = subirArchivo('avatar', __DIR__ . '/../images/');

$usuario = $_POST;
$usuario['contrasena'] = password_hash($_POST['contrasena'], PASSWORD_DEFAULT);
$usuario['avatar'] = $nombreImagen;

$usuarioJson = json_encode($usuario);

file_put_contents(__DIR__ . '/../db/usuarios.json', $usuarioJson);
