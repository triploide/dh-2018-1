<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Rent a Movie</title>
    <meta name="description" content="Todas las películas en un solo lugar">
    <?php include 'tpl/partials/head.tpl'; ?>
</head>
<body>
    <?php include 'tpl/partials/navbar.tpl'; ?>

    <main class="container">
      <?php
        $usuarioJson = file_get_contents('db/usuarios.json');
        $usuario = json_decode($usuarioJson, true);
      ?>
      
      <h1><?php echo $usuario['nombre'] ?></h1>
      
      <img class="img-responsive" src="images/<?php echo $usuario['avatar'] ?>" alt="">
        
    </main>

    <?php include 'tpl/partials/footer.tpl'; ?>

    <?php include 'tpl/partials/scripts.tpl'; ?>

</body>
</html>