<?php

Route::get('/', function () {
    return view('welcome');
});

Route::view('ejemplos/front/producto', 'ejemplos.front.product');

Route::view('ejemplos/admin/listado', 'ejemplos.admin.index');
Route::view('ejemplos/admin/formulario', 'ejemplos.admin.form');

Route::resource('admin/products', 'Admin\ProductController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
