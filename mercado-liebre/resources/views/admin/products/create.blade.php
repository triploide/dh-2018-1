@extends('admin.app')

@section('title', 'Formulario de producto')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Productos</a></li>
            <li class="breadcrumb-item active" aria-current="page">Listado</li>
        </ol>
    </nav>

    <br>
@endsection

@section('content')
    <h1>Formulario</h1>
    <br>

    <form method="post" action="/admin/products" enctype="multipart/form-data">
        @csrf
        {{-- method_field('PUT') --}}
        <div class="form-group row">
            <label for="name" class="col-sm-3 col-form-label">Nombre</label>
            <div class="col-sm-9">
                <input name="name" type="text" class="form-control" id="name" placeholder="Nombre">
            </div>
        </div>

        <div class="form-group row">
            <label for="description" class="col-sm-3 col-form-label">Descripción</label>
            <div class="col-sm-9">
                <textarea name="description" class="form-control" id="description"></textarea>
            </div>
        </div>

        <div class="form-group row">
            <label for="price" class="col-sm-3 col-form-label">Precio</label>
            <div class="col-sm-9">
                <input name="price" type="text" class="form-control" id="price" placeholder="Precio">
            </div>
        </div>

        <div class="form-group row">
            <label for="category_id" class="col-sm-3 col-form-label">Categoría</label>
            <div class="col-sm-9">
                <select name="category_id" class="form-control" id="category_id">
                    @foreach ($categories as $category)
                        <option value="{{$category->id}}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="select" class="col-sm-3 col-form-label">Imagen</label>
            <div class="col-sm-9">
                <div class="custom-file"> 
                    <input name="image" type="file" class="custom-file-input" id="image">
                    <label class="custom-file-label" for="image">Elija una imagen...</label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="select" class="col-sm-3 col-form-label">Tags</label>
            <div class="col-sm-9 d-flex justify-content-between">
                @foreach ($tags as $tag)
                    <div class="custom-control custom-checkbox">
                        <input name="tags[]" value="{{$tag->id}}" type="checkbox" class="custom-control-input" id="id{{$tag->id}}">
                        <label class="custom-control-label" for="id{{$tag->id}}">{{$tag->name}}</label>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="form-group row">
            <label for="price" class="col-sm-3 col-form-label"></label>
            <div class="col-sm-9">
                <input name="enviador" type="submit" class="btn btn-primary">
            </div>
        </div>


    </form>
@endsection


