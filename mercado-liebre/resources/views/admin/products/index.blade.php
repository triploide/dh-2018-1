@extends('admin.app')

@section('title', 'Listado de productos')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Productos</a></li>
            <li class="breadcrumb-item active" aria-current="page">Listado</li>
        </ol>
    </nav>

    <br>
@endsection

@section('content')
    <h1>Productos</h1>
    <br>

    @if (session()->has('message'))
        <div class="alert alert-success">{{ session()->get('message') }}</div>
    @endif

    <table class="table table-striped table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Imagen</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Precio</th>
                        <th scope="col">Categoría</th>
                        <th scope="col">Tags</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td scope="col"><img width="80" src="/content/products/{{$product->thumb()->src}}" ></td>
                            <td scope="col">{{ $product->name }}</td>
                            <td scope="col">${{ $product->price }}</td>
                            <td scope="col">{{ $product->category->name }}</td>
                            <td scope="col">{{ $product->tags()->pluck('name')->implode(', ') }}</td>
                            <td scope="col">
                                <a href="/admin/products/{{$product->id}}/edit" class="btn btn-primary">Editar</a>
                                <form method="post" action="/admin/products/{{$product->id}}">
                                    @csrf
                                    {{ method_field('DELETE') }}
                                        <input name="borrar" type="submit" value="Borrar" href="" class="btn btn-danger">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
@endsection


