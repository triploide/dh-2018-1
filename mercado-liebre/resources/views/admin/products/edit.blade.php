@extends('admin.app')

@section('title', 'Formulario de producto')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Productos</a></li>
            <li class="breadcrumb-item active" aria-current="page">Listado</li>
        </ol>
    </nav>

    <br>
@endsection

@section('content')
    <h1>Formulario</h1>
    <br>

    {{ Form::model($product, ['route' => ['products.update', $product->id], 'method' => 'put', 'files' => true]) }}
        @csrf
        {{-- method_field('PUT') --}}
        <div class="form-group row">
            <label for="name" class="col-sm-3 col-form-label">Nombre</label>
            <div class="col-sm-9">
                {{ Form::text('name', null, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group row">
            <label for="description" class="col-sm-3 col-form-label">Descripción</label>
            <div class="col-sm-9">
                {{ Form::textarea('description', null, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group row">
            <label for="price" class="col-sm-3 col-form-label">Precio</label>
            <div class="col-sm-9">
                {{ Form::text('price', null, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group row">
            <label for="category_id" class="col-sm-3 col-form-label">Categoría</label>
            <div class="col-sm-9">
                {{ Form::select('category_id', $categories, null, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group row">
            <label for="select" class="col-sm-3 col-form-label">Imagen</label>
            <div class="col-sm-9">
                <div class="custom-file"> 
                    <input name="image" type="file" class="custom-file-input" id="image">
                    <label class="custom-file-label" for="image">Elija una imagen...</label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="select" class="col-sm-3 col-form-label">Tags</label>
            <div class="col-sm-9 d-flex justify-content-between">
                @foreach ($tags as $tag)
                    <div class="custom-control custom-checkbox">
                        {{ Form::checkbox('tags[]', $tag->id, null, ['class' => 'custom-control-input']) }}
                        <label class="custom-control-label" for="id{{$tag->id}}">{{$tag->name}}</label>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="form-group row">
            <label for="price" class="col-sm-3 col-form-label"></label>
            <div class="col-sm-9">
                <input name="enviador" type="submit" class="btn btn-primary">
            </div>
        </div>
    {{ Form::close() }}
@endsection


