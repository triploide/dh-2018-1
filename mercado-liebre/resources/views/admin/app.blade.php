<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">
    <title>@yield('title') - {{ config('app.name') }}</title>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>

    <div class="container">

        @include('admin.partials.nav')

        @yield('breadcrumb')

        <main style="padding-bottom: 60px">
            @yield('content')           
        </main>
    </div>

    @include('admin.partials.footer')

    <script src="/js/app.js"></script>
</body>
</html>