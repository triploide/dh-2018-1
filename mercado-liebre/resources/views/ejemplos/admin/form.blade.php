<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">
    <title>Ejemplo de listado de entidades</title>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>

    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #002726">
            <a class="navbar-brand" href="#">
                <img src="/images/logo.jpg" width="40" alt="Mercado Liebre">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Dashboard <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Productos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Categorías</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Ventas</a>
                    </li>
                </ul>
            </div>

            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Usuario
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Perfil</a>
                        <a class="dropdown-item" href="#">Estadísticas</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>

        <br>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Productos</a></li>
                <li class="breadcrumb-item active" aria-current="page">Listado</li>
            </ol>
        </nav>

        <br>

        <main>
            <h1>Formulario</h1>
            <br>

            <form>
                <div class="form-group row">
                    <label for="password" class="col-sm-3 col-form-label">Password</label>
                    <div class="col-sm-9">
                        <input name="password" type="password" class="form-control" id="password" placeholder="Password">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="select" class="col-sm-3 col-form-label">Select</label>
                    <div class="col-sm-9">
                        <select name="select" class="form-control" id="select">
                            <option value="1">Uno</option>
                            <option value="2">Dos</option>
                            <option value="3">Tres</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="select" class="col-sm-3 col-form-label">Imagen</label>
                    <div class="col-sm-9">
                        <div class="custom-file"> 
                            <input name="image" type="file" class="custom-file-input" id="image">
                            <label class="custom-file-label" for="image">Elija una imagen...</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="select" class="col-sm-3 col-form-label">Checkbox</label>

                    <div class="col-sm-9 d-flex justify-content-between">
                        <div class="custom-control custom-checkbox">
                            <input name="opciones[]" value="1" type="checkbox" class="custom-control-input" id="uno">
                            <label class="custom-control-label" for="uno">Uno</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input name="opciones[]" value="2" type="checkbox" class="custom-control-input" id="dos">
                            <label class="custom-control-label" for="dos">Dos</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input name="opciones[]" value="3" type="checkbox" class="custom-control-input" id="tres">
                            <label class="custom-control-label" for="tres">Tres</label>
                        </div>
                    </div>
                </div>


            </form>           
        </main>
    </div>

    <footer class="fixed-bottom" style="padding: 20px; background-color: #333">
        <div class="container">
            <div class="row">
                <div class="col text-light">Mercado Liebre 2018</div>
            </div>
        </div>
    </footer>

    <script src="/js/app.js"></script>
</body>
</html>