<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">
    <title>Ejemplo de listado de entidades</title>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>

    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #002726">
            <a class="navbar-brand" href="#">
                <img src="/images/logo.jpg" width="40" alt="Mercado Liebre">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Dashboard <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Productos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Categorías</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Ventas</a>
                    </li>
                </ul>
            </div>

            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Usuario
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Perfil</a>
                        <a class="dropdown-item" href="#">Estadísticas</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>

        <br>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Productos</a></li>
                <li class="breadcrumb-item active" aria-current="page">Listado</li>
            </ol>
        </nav>

        <br>

        <main>
            <h1>Listado</h1>
            <br>

            <table class="table table-striped table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Col 1</th>
                        <th scope="col">Col 2</th>
                        <th scope="col">Col 3</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td scope="col">Col 1</td>
                        <td scope="col">Col 2</td>
                        <td scope="col">Col 3</td>
                    </tr>
                    <tr>
                        <td scope="col">Col 1</td>
                        <td scope="col">Col 2</td>
                        <td scope="col">Col 3</td>
                    </tr>
                    <tr>
                        <td scope="col">Col 1</td>
                        <td scope="col">Col 2</td>
                        <td scope="col">Col 3</td>
                    </tr>
                </tbody>
            </table>
        </main>
    </div>

    <footer class="fixed-bottom" style="padding: 20px; background-color: #333">
        <div class="container">
            <div class="row">
                <div class="col text-light">Mercado Liebre 2018</div>
            </div>
        </div>
    </footer>

    <script src="/js/app.js"></script>
</body>
</html>