<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Image;
use App\Product;
use App\Tag;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.products.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::create(request()->all());
        $product->tags()->sync(request()->input('tags'));
        $this->addImage($product);
        session()->flash('message', 'El producto se creó con éxito');
        return redirect('admin/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::pluck('name', 'id');
        $tags = Tag::all();
        return view('admin.products.edit', compact('product', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Product $product)
    {
        $product->update(request()->all());
        $product->tags()->sync(request()->input('tags'));
        $this->addImage($product);
        session()->flash('message', 'El producto se editó con éxito');
        return redirect('admin/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        session()->flash('message', 'El producto se borró con éxito');

        //session()->all();
        //session()->get('clave');
        //session()->put('deleted', 'true');
        //session()->push('clave', 'valor'); //tipo array

        return redirect('admin/products');
    }

    private function addImage($product)
    {
        if (request()->hasFile('image')) {
            $imageName = $product->slug . '.' . request()->file('image')->getClientOriginalExtension();
            \Intervention::make(request()
                ->file('image'))
                ->fit(900, 600)
                ->save(public_path('content/products/'.$imageName))
            ;

            $image = new Image(['src' => $imageName, 'product_id' => $product->id]);
            $image->save();
        }
    }
}
