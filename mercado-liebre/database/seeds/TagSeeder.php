<?php

use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
        	['name' => 'Aire Libre', 'slug' => 'aire-libre'],
        	['name' => 'Montaña', 'slug' => 'montana'],
        	['name' => 'Tegnología', 'slug' => 'tegnologia']
        ]);
    }
}
