<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
        	['name' => 'Electrodomésticos', 'slug' => 'electrodomesticos'],
        	['name' => 'Zapatillas', 'slug' => 'zapatillas'],
        	['name' => 'Muebles', 'slug' => 'muebles']
        ]);
    }
}
