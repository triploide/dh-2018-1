<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Products
        Schema::create('products', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->text('description');
            $table->float('price')->unsigned();
            $table->softdeletes();
            $table->timestamps();

            $table->smallInteger('category_id')->unsigned()->index();
        });

        //Categories
        Schema::create('categories', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->softdeletes();
            $table->timestamps();
        });

        //Tag
        Schema::create('tags', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->softdeletes();
            $table->timestamps();
        });

        //Purchase
        Schema::create('purchase', function (Blueprint $table) {
            $table->increments('id');
            $table->float('total')->unsigned();
            $table->boolean('state')->default(0);
            $table->softdeletes();
            $table->timestamps();
        });

        //Images
        Schema::create('images', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string('src');
            $table->timestamps();

            $table->mediumInteger('product_id')->unsigned()->index();
        });

        //Items
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->float('price')->unsigned();
            $table->float('total')->unsigned();
            $table->tinyInteger('quantity')->unsigned();
            $table->timestamps();

            $table->mediumInteger('product_id')->unsigned()->index();
            $table->integer('purchase_id')->unsigned()->index();
        });

        //Products - Tags
        Schema::create('product_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumInteger('product_id')->unsigned()->index();
            $table->smallInteger('tag_id')->unsigned()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('tags');
        Schema::dropIfExists('purchase');
        Schema::dropIfExists('images');
        Schema::dropIfExists('items');
        Schema::dropIfExists('product_tag');
    }
}
