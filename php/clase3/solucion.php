<?php

echo '<h1>Clase 2</h1>';

$numero = rand(1,3);

$colores = [
  1 => 'Rojo',
  2 => 'Azul',
  3 => 'Amarillo'
];

$color = $colores[$numero];

var_dump($color);

switch ($color) {
  case 'Rojo':
    echo 'La remera es roja';
    break;
  case 'Azul':
      echo 'La remera es azul';
      break;
  default:
    echo 'La remera es amarilla';
    break;
}

echo '<h1>Clase 3</h1>';

$caras = 0;
$tiros = 0;

while ($caras < 5) {
  $moneda = rand(0,1);
  $tiros++;

  if ($moneda == 1) {
    $caras++;
  }
}

echo "$tiros<br>";
echo "$caras<br>";






