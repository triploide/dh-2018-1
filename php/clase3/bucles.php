<?php

echo '<h1>While</h1>';

$inicio = 1;

while ($inicio < 10) {
  echo "$inicio <br>";
  $inicio++;
}

var_dump($inicio);

echo '<hr>';

echo '<h1>Do While</h1>';

$inicio = 1;

do {
  echo "$inicio <br>";
  $inicio++;
} while ($inicio < 1);

echo '<h1>Ejemplo</h1>';

$plutonio = true;
$velocidad = 50;

while ($velocidad <= 88) {
  $velocidad++;
}

//viaje en el tiempo
if ($plutonio && $velocidad >= 88) {
  echo "Viajar en el tiempo";
}

echo '<h1>For</h1>';

for ($inicio = 1; $inicio < 10; $inicio++) {
  echo "$inicio <br>";
}

echo "$inicio<br>";

echo '<h1>Break</h1>';

$par = null;

for ($inicio = 1; $inicio < 10; $inicio++) {
  if ($inicio  %  2 == 0) {
    $par = $inicio;
    break;
  }
}

if ($par) {
  echo "$par<br>";
}

echo '<h1>Continue</h1>';

for ($inicio = 1; $inicio < 10; $inicio++) {
  if ($inicio % 2 == 0) {
    echo "Es par<br>";
    continue;
  }
  echo "Dentro del bucle<br>";
}

echo '<h1>Foreach</h1>';

$colores = ['Rojo', 'Azul', 'Amarillo'];

foreach ($colores as $color) {
  echo "$color<br>";
}

echo "<br>";

$auto = [
  'color' => 'Verde',
  'marca' => 'Ford',
  'anio' => 2007
];

foreach ($auto as $key => $value) {
  echo "$key: $value<br>";
}

echo "<br>";

$categorias = [
  'peliculas' => ['Comedia', 'Drama'],
  'series' => ['Animadas', 'Sitcom']
];

foreach ($categorias as $categoria => $subcategorias) {
  echo "<h3>$categoria</h3>";
  echo "<ul>";
  foreach ($subcategorias as $subcategoria) {
    echo "<li>$subcategoria</li>";
  }
  echo "</ul>";
}


echo "<br>";

$numeros = [2,5,8,10];

do {
  $numero = rand(1,10);
} while (in_array($numero, $numeros));

$numeros[] = $numero;



echo '<br>';
echo '<br>';
echo '<br>';
echo '<br>';
echo '<br>';
echo '<br>';



