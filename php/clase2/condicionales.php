<?php

$variable = "hola";

if ($variable) {
  //echo "Dentro del if<br>";
}

$presupuesto = 10;
$esTarde = false;
$costoTaxi = 100;
$ganasCaminar = true;

if ( ($esTarde && $presupuesto >= $costoTaxi) || $ganasCaminar) {
  echo "Voy en taxi<br>";
}

$numero1 = 42;
$numero2 = 17;

if ($numero1 != $numero2) {
  //echo "Son distintos";
}

//Ejerecicio

$velocidad = 88;
$hayPlutonio = true;

if ($hayPlutonio && $velocidad >= 88) {
  //echo "viajamos en el tiempo";
}

$edad = 25;
$soltera = false;

if ($edad >= 18 && !$soltera) {
  echo "Hola Señora<br>";
} else {
  echo "Hola señorita";
}

if ($usuario = "Bort") {
  echo "Hola $usuario<br>";
}

$usuario = 'admin';

/*
if ($usuario == 'admin') {
  $saludo = "Hola admin";
} else {
  $saludo = 'Hola usuario';
}
*/

$saludo = ($usuario == 'admin') ? 'Hola admin' : 'Hola usuario';

echo $saludo.'<br>';

$dia = 3;

if ($dia == 1) {
  echo "Hoy es domingo";
} else if ($dia == 2) {
  echo "Hoy es lunes";
}

switch ($dia) {
  case 1:
    echo "Hoy es domingo";
    break;
  case 2:
      echo "Hoy es lunes";
      break;
  case 3:
    echo "Hoy es martes";
    break;
  default:
    echo "El día no es correcto";
    break;
}

$dias = [
  1 => 'Domingo',
  2 => 'Lunes',
  3 => 'Martes'
];

echo "Hoy es " . $dias[$dia];


echo "<hr>";

$nombre = "Bort";
echo "Hola $nombre<br>";
echo 'Hola $nombre<br>';

echo "\n";

echo '<h1 class="titulo">Hola \'mundo\'</h1>';
echo "\n";
echo "<h1 class=\"titulo\">Hola 'mundo'</h1>";

$texto1 = "hola";
$texto2 = 'mundo';

$texto = $texto1 . " " . $texto2;
echo $texto;




















