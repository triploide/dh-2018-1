<?php
$condicion_a_evaluar = true;

//condicional if
if ($condicion_a_evaluar) {
    echo "La condición evaluada es verdadera<br>";
}

//------------------------------------

$condicion_a_evaluar = false;

//condicional if else
if ($condicion_a_evaluar) {
    echo "La condición evaluada es verdadera<br>";
} else {
    echo "La condición evaluada es falsa<br>";
}

//------------------------------------

$primera_condicion_a_evaluar = false;

$segunda_condicion_a_evaluar = true;

//condicional if elsif
if ($primera_condicion_a_evaluar) {
    echo "La primera condición evaluada es verdadera<br>";
} elseif ($segunda_condicion_a_evaluar)  {
    echo "La segunda condición evaluada es verdadera<br>";
}

//------------------------------------

$primera_condicion_a_evaluar = false;

$segunda_condicion_a_evaluar = false;

//condicional if elsif else
if ($primera_condicion_a_evaluar) {
    echo "La primera condición evaluada es verdadera<br>";
} elseif ($segunda_condicion_a_evaluar)  {
    echo "La segunda condición evaluada es verdadera<br>";
} else {
    echo "Ninguna de las condiciones evaluadas es verdadera<br>";
}

//TIP: mostrar variables dentro de una cadena de texto
$nombre = "Bort";
echo "Hola $nombre";


