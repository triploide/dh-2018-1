<?php
$nombre = "juan";
//$edad = 25;
$edad = 25.0;

var_dump($edad);

$nulo = null;

//Arrays

$nombres = [];
$nombres[] = 'Barry';
$nombres[2] = 76;
$nombres[] = 42;
$nombres[10] = 50;

$nombres = [
  "Barry",
  "Barclay",
  "Bert",
  10 =>"Bort"
];

var_dump($nombres);

$nombres[3] = "Bart";

echo $nombres[3];

/*
$tablasMultiplicar = [];
$tablasMultiplicar[1] = [];
$tablasMultiplicar[1][] = 1;
$tablasMultiplicar[1][] = 2;
$tablasMultiplicar[1][] = 3;

$tablasMultiplicar[2] = [];
$tablasMultiplicar[2][] = 2;
$tablasMultiplicar[2][] = 4;
$tablasMultiplicar[2][] = 6;
*/

$tablasMultiplicar = [
  1 => [1, 2, 3],
  2 => [2, 4, 6],
  3 => [3, 6, 9]
];

var_dump($tablasMultiplicar[3][1]);

$auto = [
  'colores' => ['Verde', 'Negro'],
  'marca' => 'Ford',
  'anio' => 2007
];

var_dump($auto);
