<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Formulario</title>
  </head>
  <body>
    <form action="php/cookie.controller.php" method="post">
      <p>
        <label for="email">Email</label>
        <input type="email" name="email" id="email">
      </p>
      <p>
        <label for="password">Password</label>
        <input type="password" id="password" name="password">
      </p>
      <p>
        <button type="submit" name="login">Login</button>
      </p>
    </form>
  </body>
</html>