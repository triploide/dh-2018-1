<?php
session_start();
if (!isset($_SESSION['i'])) {
  $_SESSION['i'] = 0;
}
$_SESSION['i']++;
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Contador</title>
  </head>
  <body>
    <h1>i vale: <?php echo $_SESSION['i'] ?></h1>
    <p><a href="contador.php">Sumar</a></p>
  </body>
</html>