<?php session_start(); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Archivo 1</title>
  </head>
  <body>
    <h1>Archivo 2</h1>
    <?php if (isset($_SESSION['email'])): ?>
      <p>El mail del usuario es: <?php echo $_SESSION['email'] ?></p>
    <?php endif; ?>
    <?php if (isset($_COOKIE['clave'])): ?>
      <p>El mail del usuario es: <?php echo $_COOKIE['clave'] ?></p>
    <?php endif; ?>
    <p><a href="perfil.php">Perfil</a></p>
    <p><a href="archivo1.php">Archivo 1</a></p>
    <p><a href="form.php">Formulario</a></p>
    <p><a href="php/logout.controller.php">Cerrar sesión</a></p>
  </body>
</html>