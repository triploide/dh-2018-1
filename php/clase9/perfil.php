<?php
session_start();
if (!isset($_SESSION['email'])) {
  header('Location: form.php');
  exit;
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Perfil</title>
  </head>
  <body>
    <h1>Perfil</h1>
    <p><a href="archivo1.php">Archivo 1</a></p>
    <p><a href="archivo2.php">Archivo 2</a></p>
    <p><a href="form.php">Formulario</a></p>
    <p><a href="php/logout.controller.php">Cerrar sesión</a></p>
  </body>
</html>