<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Registro</title>
    <style media="screen">
      div {
        margin: 20px
      }
    </style>
  </head>
  <body>
    <form action="controllers/usuario.controller.php" method="post" enctype="multipart/form-data">
      <div>
        <label for="email">Email</label>
        <input id="email" type="email" name="email" value="">
      </div>
      <div>
        <label for="avatar">Avatar</label>
        <input type="file" name="avatar" value="">
      </div>
      <div>
        <label for="cv">CV</label>
        <input type="file" name="cv" value="">
      </div>
      <div>
        <button type="submit" name="button">Enviar</button>
      </div>
    </form>
  </body>
</html>