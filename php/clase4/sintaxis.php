<?php

//Declarar una función
function saludar($nombre)
{
    echo "Hola $nombre";
}

//Lammar una función
saludar('Juan');

//----------

//función con parámetros opcionales
function titulo($texto, $tag='h1')
{
    echo "<$tag>$texto</$tag>";
}

titulo('Funciones');
titulo('Ejemplo 2', 'h3');

//Recuperar el valor de una variable global dentro de una función
$alumnos = 10;

function porcentajeDeAusentes($ausentes)
{
    global $alumnos;
    return $ausentes * 100 / $alumnos . '%';
}

echo porcentajeDeAusentes(2);
echo '<br>';

//Función con valor estático
function incrementar()
{
    static $i = 0;
    $i++;
    return $i;
}

echo incrementar();
echo '<br>';
echo incrementar();
echo '<br>';
echo incrementar();

echo '<hr>';

//FUNCIONES ÚTILES DE PHP
$array = ['Alpha', 'Bravo', 'Charlie', 'Delta', 'Echo'];

//count: devuelve el largo de un array
echo count($array);
echo '<br>';

//in_array($elemento, $array): devulve true si el elemento está en el array, false caso contrario
var_dump(in_array('Charlie', $array));
var_dump(in_array('Foxtrot', $array));

//array_push($array, $elemento): agrega un elemento en la última posición del array;
array_push($array, 'Foxtrot');
var_dump($array);

//array_pop($array): elimina el elemento último elemento del array;
array_pop($array);
var_dump($array);

//sort($array): ordena un array;
$array = [4, 3, 1, 5, 2];
sort($array);
var_dump($array);

