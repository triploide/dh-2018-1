<?php

function saludar()
{
  echo "Hola<br>";
}

saludar();

//-------------------------------

function imprimir($texto)
{
  echo $texto . '<br>';
}

imprimir('hola mundo');
imprimir('hola DH');

echo '<hr>';

//-------------------------------

function calcularAusentes($ausentes, $total=10)
{
  echo ($ausentes * 100 / $total) . '%<br>' ;
}

calcularAusentes(3);
calcularAusentes(3, 50);

echo '<hr>';

//-------------------------------
function sumar($a, $b)
{
  $total = $a + $b;
  return $total;
}

$resultado = sumar(2,3) * 10;

echo $resultado;

echo '<hr>';

//--------------------------------

function despertarse($hora, $alarma)
{
  if ($hora == $alarma) {
    $esHora = true;
  } else {
    $esHora = false;
  }
  return $esHora;
}

if (despertarse(10, 10)) {
  echo "Hora de programar";
} else {
  echo "Seguir durmiendo";
}

echo '<hr>';

//-------------------------------

function restar($a, $b)
{
  $total = $a - $b;
  return $total;
}

echo restar(3,2);

$colorPrimario = 'verde';

function nav($contenido)
{
  global $colorPrimario;
  $html = '<nav class="'.$colorPrimario.'">'.$contenido.'</nav>';
  return $html;
}

echo nav('Menu');

echo '<hr>';

//-------------------------------

function comprar()
{
  static $total = 0;
  $total++;
  return $total;
}

echo comprar() . '<br>';
echo comprar() . '<br>';
echo comprar() . '<br>';
echo comprar() . '<br>';

$numeroMagico = 42;

function mayor($a, $b, $c=null)
{
  if ($c === null) {
    global $numeroMagico;
    $c = $numeroMagico;
  }
  $mayor = ($a > $b) ? $a : $b;
  if ($c > $mayor) {
    $mayor = $c;
  }
  return $mayor;
}

echo '<hr>';

echo mayor(5,7,0);

echo '<hr>';


function tabla($inicio, $fin)
{
  $array = [];
  for($i=$inicio; $i<=$fin; $i++) {
    $array[] = $i;
  }
  return $array;
}

var_dump(tabla(2,5));



















