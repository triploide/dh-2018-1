<?php

$nombre = trim($_POST['nombre']);

if (!$nombre) {
    echo 'El nombre es obligatorio<br>';
}

if (!isset($_POST['categorias'])) {
  echo 'Elegí al menos una categoría<br>';
}

$email = trim($_POST['email']);

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
  echo 'El email ingresado no es válido<br>';
}

$email_confirm = trim($_POST['email_confirm']);

if ($email != $email_confirm) {
  echo 'Los mails no coinciden<br>';
}

$descripcion = trim($_POST['descripcion']);

if (strlen($descripcion) > 255) {
  echo 'La desscripción es muy larga<br>';
}

$pass = $_POST['contrasena'];
if (!strstr($pass, '!')) {
  echo 'A tu pass le falta un !';
}


//is_array();

//is_numeric();

//in_array();




