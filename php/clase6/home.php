<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Home</title>
  </head>
  <body>
    <h1>Home</h1>
    
    <?php $paises = ['Argentina', 'Brazil', 'Chile']; ?>
    
    <form action="" method="post">
      <?php if (isset($paises)): ?>
        <label for="paises">País</label>
        <select name="pais">
          <?php foreach ($paises as $key => $pais): ?>
            <option value="<?php echo $key ?>">
              <?php echo $pais ?>
            </option>
          <?php endforeach; ?>
        </select>
      <?php endif; ?>
    </form>
    
    <?php if (false): ?>
      <p>Es verdad</p>
    <?php else: ?>
      <p>Es mentira</p>
    <?php endif; ?>
    
    <?php for($i=0; $i<3; $i++): ?>
      <p>Hola</p>
    <?php endfor; ?>
    
    <?php var_dump(isset($_GET['versionCorta'])); ?>
    
    <?php if (true): ?>
        <h1>Test</h1>
    <?php endif; ?>
    
    <?php
      
    ?>
    
    <form class="" action="index.html" method="post">
      <input type="text" name="nombre" value="<?php 
      echo (isset ($_GET["nombre"]))?$_GET["nombre"]:""; ?>">
    </form>
    
    
  </body>
</html>