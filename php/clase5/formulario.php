  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <title>Formulario</title>
    </head>
    <body>
      <form action="procesar.php" method="post">
        <label for="nombre">Nombre: </label>
        <input id="nombre" type="text" name="nombre" value=""><br>
        
        <br>
        <label for="password">Contraseña: </label>
        <input id="password" type="password" name="password" value=""><br>
        
        <br>
        <label>Hobbies: </label>
        <input type="checkbox" name="hobbies[]" value="futbol">
        Fútbol
        <input type="checkbox" name="hobbies[]" value="musica">
        Música
        <input type="checkbox" name="hobbies[]" value="arte">
        Arte
        <br>
        
        <br>
        <label>Género: </label>
        <input type="radio" name="genero" value="femenino">
        Femenino
        <input type="radio" name="genero" value="masculino">
        Masculino
        <br>
        
        <br>
        <label>País: </label>
        <select name="pais">
          <option value="">Seleccionar</option>
          <option value="ar">Argentina</option>
          <option value="br">Brazil</option>
          <option value="cl">Chile</option>
        </select>
        
        <br>
        <br>
        
        <label>Día del mes: </label>
        <select name="dia">
          <?php
            for($i=1; $i<=31; $i++) {
              echo '<option value="'.$i.'">'.$i.'</option>';
            }
          ?>
        </select>
        
        <br>
        <br>
        <button type="submit">Login</button>
      </form>
    </body>
  </html>