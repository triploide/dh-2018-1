<?php

//------------------------
//---Manejo de archivos---
//------------------------

//fopen recibe dos parámetros: el nombre del archivo que vamos a abrir y el modo; devuelve un recurso
//Los modos pueden ser (pero no limitarse a):
//'r' Apertura para sólo lectura; coloca el puntero al fichero al principio del fichero.
//'r+'Apertura para lectura y escritura; coloca el puntero al fichero al principio del fichero.
//'w' Apertura para sólo escritura; coloca el puntero al fichero al principio del fichero y trunca el fichero a longitud cero. Si el fichero no existe se intenta crear.
//'w+' Apertura para lectura y escritura; coloca el puntero al fichero al principio del fichero y trunca el fichero a longitud cero. Si el fichero no existe se intenta crear.
//'a' Apertura para sólo escritura; coloca el puntero del fichero al final del mismo. Si el fichero no existe, se intenta crear.
//'a+' Apertura para lectura y escritura; coloca el puntero del fichero al final del mismo. Si el fichero no existe, se intenta crear.
$recurso = fopen('archivo.txt', 'w');

//fread se usa para guardar leer el contenido de un archivo
//recibe dos parámetros: el recurso (obtenido con fopen) y  "la cantidad caracteres" que queremos leer
//devuelve el contenido del archivo
$texto = fread($recurso, $size);

//filesize
//recibe como parámetro el path del archivo
//devuelve el total de "carateres" que tiene un archivo
filesize('archivo.txt');

//fwrite se usa para guardar contenido en un archivo
//recibe dos parámetros: el recurso (obtenido con fopen) y el contenido a volcar en el archivo
fwrite($recurso, 'contenido a escribir');

//fclose cierra la conexión al archivo abierto con fopen
//recibe un parámetro: el recurso (obtenido con fopen)
fclose($recurso);

//file_get_contents (no requiere fopen ni fclose)
//recibe el path a un archivo
//devuelve el contenido total del archivo
$texto = file_get_contents('archivo.txt');

//file_put_contents (no requiere fopen ni fclose)
//recibe dos parámetros obligatorios y uno optativo
file_put_contents('archivo.txt', 'contenido a escribir'); //trunca el archivo
//o también
file_put_contents('archivo.txt', 'contenido a escribir', FILE_APPEND | LOCK_EX); //agrega contenido al fila del archivo
