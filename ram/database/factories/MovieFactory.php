<?php

use Faker\Generator as Faker;

$factory->define(App\Movie::class, function (Faker $faker) {
	$title = $faker->word();
    return [
        'title' => $title,
        'slug' => str_slug($title),
        'ranking' => $faker->unique()->numberBetween(1,100)
    ];
});
