<?php

use Faker\Generator as Faker;

$factory->define(App\Genre::class, function (Faker $faker) {
	$name = $faker->word();
    return [
        'name' => $name,
        'slug' => str_slug($name),
        'ranking' => $faker->unique()->numberBetween(1,100)
    ];
});
