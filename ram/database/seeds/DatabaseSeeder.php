<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(GenreSeeder::class);

    	factory(App\Movie::class)->times(5)->make()->each(function ($movie) {
    		factory(App\Genre::class)->create()->movies()->save($movie);
    	});

    }
}
