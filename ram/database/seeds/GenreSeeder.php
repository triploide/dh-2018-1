<?php

use Illuminate\Database\Seeder;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('genres')->insert([
        	[
        		'name' => 'Comic',
        		'ranking' => 1001,
        		'active' => 1
        	],
        	[
        		'name' => 'Sitcom',
        		'ranking' => 1002,
        		'active' => 0
        	]
        ]);
    }
}
