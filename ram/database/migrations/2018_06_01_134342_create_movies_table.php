<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('title', 127)->nullable();
            $table->string('slug', 127)->nullable();
            $table->tinyInteger('rating')->default(1);
            $table->tinyInteger('genre_id')->unsigned()->index();
            $table->tinyInteger('director_id')->unsigned()->index();
            $table->softDeletes();
            $table->timestamps();

            //$table->foreign('genre_id')->references('id')->on('genres');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
