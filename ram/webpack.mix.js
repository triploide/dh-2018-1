let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 //.js junta los js en un solo archivo
 //.css compila los sass y los junta en un solo archivo
 //.sytles juntos los css en un solo archivo

mix.styles([
	'resources/assets/css/estilos1.css',
	'resources/assets/css/estilos2.css',
], 'public/css/styles.css');

mix.version();
