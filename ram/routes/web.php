<?php

use App\Genre;
use App\Image;
use App\Movie;

Route::get('/', function () {
    return view('welcome');
});

Route::get('test/morph', function () {
	/*
	$image1 = Image::create(['src' => 'genre.jpg']);
	$image2 = Image::create(['src' => 'movie.jpg']);

	$genre = Genre::find(1);
	$movie = Movie::find(3);

	$genre->images()->save($image1);
	$movie->images()->save($image2);
	*/

	$genre = Genre::find(1);
	dd($genre->images->first()->src);
});


Route::get('generos', 'GenreController@index')->middleware('maxi');
Route::get('generos/{id}', 'GenreController@show');

Route::get('peliculas/{lang?}', 'MovieController@index');
Route::get('peliculas/{id}', 'MovieController@show');


//admin
Route::resource('admin/genres', 'Admin\GenreController')->middleware('auth');
Route::resource('admin/movies', 'Admin\MovieController')->middleware('auth');


Auth::routes();

Route::get('registro', 'Auth\RegisterController@showRegistrationForm')->name('register');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('maxi', function () {
	echo 'Maxi';
})->middleware('maxi');
