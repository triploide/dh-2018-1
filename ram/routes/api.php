<?php

use App\Movie;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
   if ($request->user) {

   } else {
   	return ['cod' => 401];
   }
});

Route::get('movies', function () {
	$movies = Movie::all();
	return $movies->toArray();
});

Route::get('movies/{id}', function ($id) {
	$movie = Movie::find($id);
	return $movie ? $movie->toArray() : [];
});

