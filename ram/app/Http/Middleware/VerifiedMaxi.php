<?php

namespace App\Http\Middleware;

use Closure;

class VerifiedMaxi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('maxi')) {
            return $next($request);
        } else {
            return abort(404);
        }
    }
}
