<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Movie extends Model
{
   use Sluggable;

   protected $fillable = ['title', 'rating', 'genre_id'];

	//belongs to

	//has one

	//has many

	//many to many

   public function sluggable()
   {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

   public function genre()
   {
   		//return $this->belongsTo(Genre::class, 'genre_id', 'id');
   		return $this->belongsTo(Genre::class);
   }

   public function actors()
   {
   	//return $this->belongsToMany(Actor::class, 'tabla_de_relacion', 'movie_id', 'actor_id');
   	return $this->belongsToMany(Actor::class);
   }

   public function images()
   {
   	return $this->morphMany(Image::class, 'imageable');
   }

   public function toArray()
   {
     return [
      'title' => $this->title
     ];
   }

}
