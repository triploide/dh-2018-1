<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
	protected $fillable = ['name', 'ranking'];

    public function movies()
    {
    	return $this->hasMany(Movie::class);
    }

    public function images()
   {
   	return $this->morphMany(Image::class, 'imageable');
   }
}
