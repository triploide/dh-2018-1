<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

	protected $fillable = ['src'];

	public $timestamps = false;

    public function imageable()
    {
    	return $this->morphTo();
    }
}
