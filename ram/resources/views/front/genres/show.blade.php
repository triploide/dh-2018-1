<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ $genre->name }}</title>
</head>
<body>
	<h1>{{ $genre->name }}</h1>

	<ul>
		@foreach ($topMovies as $movie)
			<li>{{ $movie->title }}</li>
		@endforeach
	</ul>
</body>
</html>