<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Géneros</title>
</head>
<body>
	<h1>Listado de géneros</h1>

	<ul>
		@foreach ($genres as $genre)
			<li>
				<a href="generos/{{$genre->id}}">{{ $genre->name }} ({{ $genre->movies()->count() }})</a>
			</li>
		@endforeach
	</ul>
</body>
</html>