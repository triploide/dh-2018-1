<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ __('movies.peliculas') }}</title>
	<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
	<h1>{{ __('movies.listado-de-peliculas') }}</h1>

	@auth
		<nav>
			<ul>
				<li><a href="#">{{ __('movies.editar') }}</a></li>
			</ul>
		</nav>
	@else
		{{ __('movies.otra-cosa') }}
	@endauth
	

	<ul>
		@foreach ($movies as $movie)
			<li><a href="peliculas/{{$movie->slug}}">{{ $movie->title }}</a> - <a href="generos/{{$movie->genre->id}}">{{ $movie->genre->name }}</a></li>
		@endforeach
	</ul>
</body>
</html>