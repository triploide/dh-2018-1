@extends('admin.app')

@section('title', 'Géneros')

@section('main')
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Género</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			@forelse ($genres as $genre)
				<tr>
					<td>{{ $genre->name }}</td>
					<td>
						<a href="#" class="btn btn-primary">
							Editar
						</a>
						<a href="" class="btn btn-danger">
							Borrar
						</a>
					</td>
				</tr>
			@empty
				<tr>
					<td colspan="2">No hay géneros cargados</td>
				</tr>
			@endforelse
		</tbody>
	</table>
	{{ $genres->links()  }}
@endsection