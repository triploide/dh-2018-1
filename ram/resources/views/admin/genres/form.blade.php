@extends('admin.app')

@section('title', 'Crear Género')

@section('main')
	<div class="row">
		<div class="col-md-12">
			
			<form action="/admin/genres" method="post" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="name">Nombre</label>
					<input class="form-control" type="text" name="name">
				</div>
				<div class="form-group">
					<label for="name">Imagen</label>
					<input class="form-control" type="file" name="image">
				</div>
				<div class="form-group">
					<input class="btn btn-primary" type="submit" name="enviador" value="Guardar">
				</div>
			</form>

		</div>
	</div>
@endsection